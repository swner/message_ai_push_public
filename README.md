# message_ai_push_public

#### 介绍
message_ai_push_public 信息智能投放平台开源版本

#### 软件架构
软件架构说明 采用 Python3.7版本 flask 服务提供接口
openAPI自动生成controller 层 代码
数据库管理采用 mysql redis neo4j 
充值模块 第三方支付管理工具。
docker 镜像启动
前端采用vue 框架进行交互展示
多线程数据投放策略。


#### 数据库职责
1. mysql 负责基础数据管理
    基础数据包括 用户数据 销售数据
#### 安装教程

1.  安装必备工具
```cmd
pip install -r requirement.txt
```
2.  xxxx

3.  xxxx


#### 运行教程
```cmd

docker build -r message_ai_push_public .
docker run 
```

#### 使用说明

1.  将多平台的短信投放平台代码进行整合
2.  针对手机号码构建用户画像 用户地区 用户点击 用户注册 四种行为的记录
3.  注册与登录 不同场景下的用户权限管理

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
